package main

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// https://godoc.org/golang.org/x/crypto/bcrypt

// User of app.
type User struct {
	gorm.Model
	Code     string `gorm:"type:varchar(25);unique_index"`
	Email    string `gorm:"type:varchar(100);unique_index"`
	Password string
}

// TableName of User struct
func (u User) TableName() string {
	return "users"
}

// CreateUser a User
func CreateUser(code string, email string, password string) *User {
	pw, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		panic(err)
	}
	u := &User{
		Code:     RandomString(15),
		Email:    email,
		Password: string(pw),
	}

	connection.Create(&u)

	return u
}

// GetUserByEmail will get find a user using their email address
func GetUserByEmail(email string) *User {
	var user User
	connection.Where("email = ?", email).First(&user)

	return &user
}

// VerifyPassword of user
func (u User) VerifyPassword(password string) bool {
	r := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))

	// Incorrect password
	if r != nil {
		return false
	}

	// Password is correct
	return true
}
