package main

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"

	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var connection *gorm.DB

// InitDB _
func InitDB(conn string) {
	rootCertPool := x509.NewCertPool()
	pem, err := ioutil.ReadFile("./ca-certificate.crt")
	if err != nil {
		log.Fatal(err)
	}
	if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
		log.Fatal("Failed to append PEM.")
	}
	mysql.RegisterTLSConfig("custom", &tls.Config{
		RootCAs:            rootCertPool,
		InsecureSkipVerify: true,
	})

	connection, err = gorm.Open("mysql", conn)

	if err != nil {
		panic(err)
	}
	connection.LogMode(false)

	connection.AutoMigrate(&User{})
	connection.AutoMigrate(&Message{})
}
