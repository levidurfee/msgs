package main

import "github.com/jinzhu/gorm"

// MessageStatus indicates the status of the message
type MessageStatus int

const (
	_ = iota
	// MessageSent indicates the message was sent, but not yet read
	MessageSent MessageStatus = 1
	// MessageRead indicates the message was opened by the recipient
	MessageRead
)

// Message is what people send to each other
type Message struct {
	gorm.Model
	FromID  uint
	From    User
	ToID    uint
	To      User
	Subject string
	Body    string `gorm:"type:mediumtext"`
	Status  MessageStatus
}

// CreateMessage creates a new message.
func CreateMessage(from User, to User, subject string, body string) {
	m := &Message{
		From:    from,
		To:      to,
		Subject: subject,
		Body:    body,
	}

	connection.Create(&m)
}
